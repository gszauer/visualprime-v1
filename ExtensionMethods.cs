﻿using System.Collections;
using System.Collections.Generic;

namespace VisualPrime {
	public static class ExtensionMethods {
		public static T[] RemoveAt<T>(this T[] arr, int index) {
			T[] nArr = new T[arr.Length - 1];

			for (int i = 0; i < index; ++i) {
				nArr[i] = arr[i];
			}

			for (int a = index; a < arr.Length - 1; a++) {
				nArr[a] = arr[a + 1];
			}

			return nArr;
		}

		public static int FindIndex<T>(this T[] arr, T item) {
			for (int i = 0; i < arr.Length; ++i) {
				if (EqualityComparer<T>.Default.Equals(arr[i], item)) {
					return i;
				}
			}

			return -1;
		}

		public static T[] Append<T>(this T[] arr, T item) {
			T[] nArr = new T[arr.Length + 1];

			System.Array.Copy(arr, nArr, arr.Length);
			nArr[arr.Length] = item;

			return nArr;
		}
	}
}