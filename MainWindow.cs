﻿using System;
using Gtk;
using VisualPrime;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;

public partial class MainWindow: Gtk.Window {
	public VirtualMachine _vm;
	public VirtualMachine.ExecutionState _state;
	public string _textBuffer = null;

	public TextView _commandView = null;
	public Label _outputText = null;
	public List<Label> _cellLabels = null;
	public List<Frame> _cellFrames = null;
	public Entry _inputStream = null;
	public Button _execute = null;
	public Button _step = null;
	public Button _stop = null;
	public uint _timerID = 0;
	public Label _markupLabel = null;
	public HBox _programLayout = null;

	public MainWindow () : base (Gtk.WindowType.Toplevel) {
		Gtk.Window mainWindow = new Gtk.Window ("Visual Prime");
		mainWindow.SetSizeRequest (800,600);
		mainWindow.DeleteEvent += (object sender, DeleteEventArgs a) => {
			Application.Quit ();
			a.RetVal = true;
			Destroy();
		};

		_cellLabels = new List<Label> ();
		_cellFrames = new List<Frame> ();
		mainWindow.Add (CreateLayout (mainWindow));

		mainWindow.ShowAll ();
	}

	VBox CreateLayout(Gtk.Window window) {
		VBox verticalLayout = new VBox ();

		HBox cellsLayout = new HBox ();
		cellsLayout.HeightRequest = 50;
		for (int i = 0; i < 10; ++i) {
			Frame frame = new Frame ();
			Label label = new Label ("0");
			label.WidthChars = 3;
			label.MaxWidthChars = 3;
			frame.Add (label);
			_cellLabels.Add (label);
			_cellFrames.Add (frame);
			cellsLayout.PackStart (frame);
		}

		HBox inputLayout = new HBox ();
		inputLayout.PackStart(new Label("Input stream:"), false, true, 2);
		_inputStream = new Entry ();
		_inputStream.Changed += (object sender, EventArgs e) => {
			string ns = "";
			foreach(char c in _inputStream.Text) {
				if (c == ' ' || Char.IsDigit(c)) {
					ns += c;
				}
			}
			_inputStream.Text = ns;
		};
		inputLayout.PackStart (_inputStream);

		HBox outputLayout = new HBox ();
		outputLayout.PackStart (new Label ("Output stream:"), false, true, 2);
		_outputText = new Label ("");
		_outputText.SetAlignment (0.0f, 0.5f);
		outputLayout.PackStart (_outputText);

		HBox commandLayout = new HBox ();
		_execute = new Button (Image.LoadFromResource ("VisualPrime.control_play.png"));
		_execute.Clicked += (object obj, EventArgs args) => { OnExecute(); };
		commandLayout.PackStart(_execute, false, true, 1);
		_step = new Button (Image.LoadFromResource ("VisualPrime.control_end.png"));
		_step.Clicked += (object obj, EventArgs args) => { OnStep (); };
		commandLayout.PackStart(_step, false, true, 1);
		_stop = new Button (Image.LoadFromResource ("VisualPrime.control_stop.png"));
		_stop.Clicked += (object obj, EventArgs args) => { OnStop (); };
		commandLayout.PackStart(_stop, false, true, 1);
		commandLayout.PackStart (cellsLayout);

		_programLayout = new HBox ();
		_commandView = new TextView ();
		_programLayout.Add(_commandView);
		_markupLabel = new Label ();
		_markupLabel.SetAlignment (0.0f, 0.0f);

		verticalLayout.PackStart (inputLayout, false, true, 1);
		verticalLayout.PackStart (commandLayout, false, true, 1);
		verticalLayout.PackStart (outputLayout, false, true, 1);
		verticalLayout.PackStart (new HSeparator(), false, true, 1);
		verticalLayout.PackStart (_programLayout, true, true, 2);

		return verticalLayout;
	}

	void HookupVm() {
		_vm.CellChanged = (int index, int value) => {
			foreach(Label label in _cellLabels) 
				label.Markup = label.Text;
			_cellLabels [index].Markup = "<b><span foreground=\"red\">" + value.ToString() + "</span></b>";

			Gdk.Color col = new Gdk.Color();
			foreach(Frame frame in _cellFrames)
				frame.ModifyBg(StateType.Normal);
			Gdk.Color.Parse("red", ref col);
			_cellFrames[index].ModifyBg(StateType.Normal, col);
		};

		_vm.CellSelected = (int index) => {
			Gdk.Color col = new Gdk.Color();

			foreach(Frame frame in _cellFrames)
				frame.ModifyBg(StateType.Normal);

			Gdk.Color.Parse("red", ref col);
			_cellFrames[index].ModifyBg(StateType.Normal, col);

			foreach(Label label in _cellLabels) 
				label.Markup = label.Text;
			_cellLabels [index].Markup = "<b><span foreground=\"red\">" + _cellLabels [index].Text.ToString() + "</span></b>";

		};

		_vm.OutputChanged += (string output) => {
			_outputText.Text = output;
		};

		_vm.Finished = () => {
			_step.Sensitive = true;
			_execute.Sensitive = true;
		};

		_vm.Error = (string error) => {
			OnStop();

			MessageDialog md = new MessageDialog(this,  DialogFlags.DestroyWithParent, MessageType.Error,  ButtonsType.Close, error);
			md.Run();
			md.Destroy();
		};

		_vm.CommandStarted = (int cmdIndex) => {
			string label =  System.Security.SecurityElement.Escape(_textBuffer.Substring(0, cmdIndex));
			label += "<b><span foreground=\"red\">";
			label +=  System.Security.SecurityElement.Escape(_textBuffer[cmdIndex].ToString());
			label += "</span></b>";
			label +=  System.Security.SecurityElement.Escape(_textBuffer.Substring(cmdIndex + 1, _textBuffer.Length - (cmdIndex + 1)));

			_markupLabel.Markup = label;
		};
	}

	void OnExecute(bool startTimer = true) {
		if (string.IsNullOrEmpty (_commandView.Buffer.Text))
			return;

		string[] inputs = new string[0];
		if (!string.IsNullOrEmpty(_inputStream.Text))
			inputs = _inputStream.Text.Split (' ');
		List<int> inputList = new List<int> ();
		foreach (string input in inputs)
			inputList.Add (System.Convert.ToInt32 (input));

		_outputText.Text = "";
		for (int i = 0, isize = _cellLabels.Count; i < isize; ++i)
			_cellLabels [i].Text = "0";
		_vm = new VirtualMachine (_commandView.Buffer.Text, inputList.Count == 0? null : inputList.ToArray());
		HookupVm ();
		_vm.CellChanged (0, 0);

		_textBuffer = _commandView.Buffer.Text;
		if (_programLayout.Children.FindIndex(_commandView) >= 0)
			_programLayout.Remove (_commandView);
		if (_programLayout.Children.FindIndex(_markupLabel) < 0)
			_programLayout.Add (_markupLabel);
		_markupLabel.Show ();

		_vm.CommandStarted (0);

		if (startTimer) {
			_step.Sensitive = false;
			_execute.Sensitive = false;

			_timerID = GLib.Timeout.Add (1000, new GLib.TimeoutHandler (() => {
				if (_vm == null)
					return false;

				_state = _vm.Execute (_state);

				if (_state == null) {
					_vm = null;
					_step.Sensitive = true;
					_execute.Sensitive = true;

					if (_programLayout.Children.FindIndex (_commandView) < 0)
						_programLayout.Add (_commandView);
					if (_programLayout.Children.FindIndex (_markupLabel) >= 0)
						_programLayout.Remove (_markupLabel);
				}

				return _state != null;
			}));
		}
	}

	void OnStop() {
		_vm = null;
		_state = null;

		if (_timerID != 0)
			GLib.Source.Remove (_timerID);

		_step.Sensitive = true;
		_execute.Sensitive = true;
		_outputText.Text = "";

		for (int i = 0, isize = _cellLabels.Count; i < isize; ++i)
			_cellLabels [i].Markup = "0";

		foreach(Frame frame in _cellFrames)
			frame.ModifyBg(StateType.Normal);

		if (_programLayout.Children.FindIndex(_commandView) < 0)
			_programLayout.Add (_commandView);
		if (_programLayout.Children.FindIndex(_markupLabel) >= 0)
			_programLayout.Remove (_markupLabel);
	}

	void OnStep() {
		if (string.IsNullOrEmpty (_commandView.Buffer.Text))
			return;

		_execute.Sensitive = false;

		if (_vm == null)
			OnExecute (false);

		_state = _vm.Execute (_state);

		if (_state == null) {
			_vm = null;
			_step.Sensitive = true;
			_execute.Sensitive = true;

			if (_programLayout.Children.FindIndex (_commandView) < 0)
				_programLayout.Add (_commandView);
			if (_programLayout.Children.FindIndex (_markupLabel) >= 0)
				_programLayout.Remove (_markupLabel);
		}
	}
}