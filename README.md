VisualPrime
===========
This is a visual tool for learning brainfuck. A few modifications have been made to the language to make it easyer for us to learn.
The language only has 10 Cells. All 8 operators are supported. Only numbers are allowed, 0 is 0, 1 is 1. To keep the code easy to follow, support for nested backets has been ommited. The play button executes live, the step button does things one step at a time. The input stream only allows numbers with each input being seperated by a whitespace. Non operator characters are skipped.
