﻿using System;

namespace VisualPrime {
	public class VirtualMachine {
		public class ExecutionState {
			public int CommandPointer = 0;
			public int InputPointer = 0;
			public int CellPointer = 0;
		};

		public delegate void OnCellChanged(int index, int value);
		public delegate void OnCellSelected(int index);
		public delegate void OnOutputChanged(string output);
		public delegate void OnError(string error);
		public delegate void OnFinished();
		public delegate void OnCommandStarted(int cmdIndex);

		public int[] Cells = new int[10];
		public int[] InputStream = null;
		public string OutputStream = "";
		public string CommandStream = "";

		public OnCellChanged CellChanged = null;
		public OnOutputChanged OutputChanged = null;
		public OnFinished Finished = null;
		public OnError Error = null;
		public OnCommandStarted CommandStarted = null;
		public OnCellSelected CellSelected = null;

		protected bool _executing = false;

		public VirtualMachine (string command, int[] input = null) {
			CommandStream = command;
			InputStream = input;
			for (int i = 0; i < 10; ++i)
				Cells [i] = 0;
		}

		public void Reset() {
			for (int i = 0; i < 10; ++i)
				Cells [i] = 0;
			InputStream = null;
			OutputStream = "";
			CommandStream = "";
		}

		public ExecutionState Execute(ExecutionState state = null) {
			if (state == null) {
				state = new ExecutionState ();
			}

			if (state.CommandPointer < 0 || state.CommandPointer >= CommandStream.Length) {
				_executing = false;
				return null;
			}

			_executing = true;

			//for (int cmdSize = CommandStream.Length; state.CommandPointer < cmdSize; ++state.CommandPointer) {
				char cmd = CommandStream [state.CommandPointer];
				if (CommandStarted != null) {
					CommandStarted (state.CommandPointer);
				}

				if (cmd == '<') {
					state.CellPointer--;
					if (state.CellPointer < 0) {
						if (Error != null) {
							Error ("Can't decrement cell pointer below 0");
						}
						return null;
					}
					if (CellSelected != null) {
						CellSelected (state.CellPointer);
					}
				} else if (cmd == '>') {
					state.CellPointer++;
					if (state.CellPointer >= 10) {
						if (Error != null) {
							Error ("Can't increment cell pointer above 9");
						}
						return null;
					}
					if (CellSelected != null) {
						CellSelected (state.CellPointer);
					}
				} else if (cmd == '+') {
					Cells [state.CellPointer]++;
					if (CellChanged != null) {
						CellChanged (state.CellPointer, Cells [state.CellPointer]);
					}
				} else if (cmd == '-') {
					Cells [state.CellPointer]--;
					if (CellChanged != null) {
						CellChanged (state.CellPointer, Cells [state.CellPointer]);
					}
				} else if (cmd == '.') {
					OutputStream += Cells [state.CellPointer].ToString ();
					if (OutputChanged != null) {
						OutputChanged (OutputStream);
					}
				} else if (cmd == ',') {
					if (InputStream == null) {
						if (Error != null) {
							Error ("No input stream detected");
						}
						return null;
					}
					if (state.InputPointer >= InputStream.Length) {
						if (Error != null) {
							Error ("Input " + state.InputPointer + ", is out of Input Stream Range");
						}
						return null;
					}
					Cells [state.CellPointer] = InputStream [state.InputPointer++];
					if (CellChanged != null) {
						CellChanged (state.CellPointer, Cells [state.CellPointer]);
					}
				} else if (cmd == '[') {
					if (Cells [state.CellPointer] == 0) {
					while (CommandStream [state.CommandPointer++] != ']') {
						if (state.CommandPointer >= CommandStream.Length) {
								if (Error != null) {
									Error ("No closing bracket (]) found");
								}
								return null;
							}
						}
					}
				} else if (cmd == ']') {
					if (Cells [state.CellPointer] != 0) {
					while (CommandStream [state.CommandPointer--] != '[') {
						if (state.CommandPointer < 0) {
								if (Error != null) {
									Error ("No opening bracket ([) found");
								}
								return null;
							}
						}
					}
				}
			//}

			state.CommandPointer++;
			return state;
		}
	}
}