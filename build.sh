#!/bin/bash
VERSION_COMMAND="mono --version | xargs echo -n | cut -c 27-31"
MONO_VERSION=`eval $VERSION_COMMAND`
echo "Mono version: $MONO_VERSION"
MONO_LIB_DIR="/Library/Frameworks/Mono.framework/Versions/$MONO_VERSION/lib/mono/"
echo "Mono lib directory: $MONO_LIB_DIR/"
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "Base directory: $BASEDIR"

CF_PATH="/Library/Frameworks/Mono.framework/Versions/Current/lib/pkgconfig"

IN_ARG="$BASEDIR/bin/Release/VisualPrime.exe"
OUT_ARG="$BASEDIR/bin/Release/VisualPrime"

export AS="as -arch i386"
export CC="cc -arch i386 -lobjc -liconv -framework Foundation"

rm "$OUT_ARG"

cd "$BASEDIR/bin/Release"

PKG_CONFIG_PATH=$CF_PATH mkbundle --static -o $OUT_ARG $IN_ARG --deps 

rm -rf "$BASEDIR/Release"
cp -rf "$BASEDIR/bin/Release" "$BASEDIR/Release"
rm -rf "$BASEDIR/bin"
rm -rf "$BASEDIR/obj"

echo "#!/bin/bash\nexport DYLD_FALLBACK_LIBRARY_PATH=/Library/Frameworks/Mono.framework/Versions/Current/lib:/lib:/usr/lib\n./VisualPrime" > "$BASEDIR/Release/OsxLauncher.sh"

cd "$BASEDIR/Release"
macpack -r:/Library/Frameworks/Mono.framework/Versions/Current/lib/ -n:VisualPrime -a:VisualPrime.exe